import {UniprotSequencesModel} from './uniprotSequences.model';
import {UniprotMappingsModel} from './uniprotMappings.model';
import {ChainInfo, DataFetcher, PdbInfo, Segment, UniprotInfo} from '../interfaces/dataFetcher.interface';
import {ResidueListsModel} from './residueLists.model';
import {PdbEntitiesModel} from './pdbEntities.model';
import {PdbEntity} from '../interfaces/pdbEntity.interface';
import {ResidueList} from '../interfaces/residueList.interface';
import {Log} from '../../_helpers/log/log.model';

export class DataFetcherModel {

  data: DataFetcher;
  uniprotSequence: UniprotSequencesModel;
  uniprotMappings: UniprotMappingsModel;
  residueLists: ResidueListsModel;
  pdbEntities: PdbEntitiesModel;

  constructor() {
    this.data = {uniprots: {}, pdbs: {}};
    this.uniprotSequence = new UniprotSequencesModel();
    this.uniprotMappings = new UniprotMappingsModel();
    this.residueLists = new ResidueListsModel();
    this.pdbEntities = new PdbEntitiesModel();
  }

  public async getData(id) {

    if (id.toString() in this.data.uniprots) {
      Log.w(2, 'data already stored in cache.');
      return undefined;
    }

    const unpSequence = await this.uniprotSequence.getData(id);
    if (!unpSequence) {
      return undefined;
    }

    const unpMapping = await this.uniprotMappings.getData(id);
    if (!unpMapping) {
      return undefined;
    }

    const uniprotInfo: UniprotInfo = {id, seq: unpSequence.sequence, pdbs: {}};
    let pdbInfo: PdbInfo;
    let chainId: string;
    let structAsymId: string;
    let chainInfo: ChainInfo;
    let segment: Segment;
    let resList: ResidueList;
    let entList: PdbEntity;
    let resListObj;
    let tmp: any;

    for (const pdb of unpMapping.pdbs) {

      uniprotInfo.pdbs[pdb.pdb] = [];
      pdbInfo = {
        uniprots: new Set<string>([unpSequence.id]),
        chains: {}
      };

      resList = await this.residueLists.getData(pdb.pdb);
      if (!resList) {
        continue;
      }
      resListObj = {};
      for (const molecule of resList.molecules) {
        if (!(molecule.entity_id in resListObj)) {
          resListObj[molecule.entity_id] = {};
        }
        tmp = resListObj[molecule.entity_id];
        for (const chain of molecule.chains) {
          if (chain.chain_id in tmp) {
            tmp[chain.chain_id] = tmp[chain.chain_id].concat(chain.residues);
          } else {
            tmp[chain.chain_id] = chain.residues;
          }
        }
      }

      for (const chain of pdb.chains) {

        chainId = chain.chain_id;
        structAsymId = chain.struct_asym_id;

        // check if chain already inserted
        // prevents to execute also for other fragments
        if (!(pdbInfo.chains.hasOwnProperty(chainId))) {

          // push inside uniprot list
          uniprotInfo.pdbs[pdb.pdb].push(chainId);


          // push inside pdbs dict
          pdbInfo.chains[chainId] = {
            regions: [],
            entity_id: undefined,
            chain_id: chainId,
            struct_asym_id: structAsymId,
            segments:[],
            pdb_res_pos: [],
            pdb_aut_res: [],
            pdb_res_num: []
          };
        }
        segment = {
          unp_end: chain.unp_end,
          unp_start: chain.unp_start,
          start_res_num: chain.start_residue_number,
          end_res_num: chain.end_residue_number
        };

        pdbInfo.chains[chainId].segments.push(segment);
        chainInfo = pdbInfo.chains[chainId];

        // find chain regions
        pdbInfo.chains[chainId].entity_id = chain.entity_id;
        entList = await this.pdbEntities.getData(pdb.pdb, chainId);

        if (!entList) {
          continue;
        }

        pdbInfo.chains[chainId].regions = entList.regions;
      }

      for (const ch of uniprotInfo.pdbs[pdb.pdb]) {
        chainInfo = pdbInfo.chains[ch];

        // calculate conversion objects
        for (const eId in resListObj) {
          if (+eId === chainInfo.entity_id) {

            tmp = resListObj[eId];

            if (ch in tmp) {

                this.pdbArrays(pdbInfo.chains[ch], unpSequence.sequence, resListObj[eId][ch]);
            }
          }
        }
      }
      // insert pdb structure inside pdbs object
      this.data.pdbs[pdb.pdb] = pdbInfo;

    }
    // insert uniprot object inside uniprots dict
    this.data.uniprots[unpSequence.id] = uniprotInfo;
    console.log(this.data)
    return this.data;
  }

  private pdbArrays(obj, unpSeq, residues) {
    const pdbResPos = [];
    const pdbResNum = [];
    const pdbAutRes = [];
    let pointer = 0;
    let flag = true;
    let segments = [];
    let segRes = [];
    for (const segment of obj.segments) {
      segments.push(segment.unp_start);
      segRes.push(segment.start_res_num);
      segments.push(segment.unp_end);
      segRes.push(segment.end_res_num);
    }
    segments = segments.sort((n1,n2) => n1 - n2);
    segRes = segRes.sort((n1,n2) => n1 - n2);


    for (let char = 1; char <= unpSeq.length; char++){

      if (char < segments[pointer]) {

        pdbResPos.push('-');
        pdbResNum.push('-');
        pdbAutRes.push('-');
      } else {
        if (char < segments[pointer + 1]) {

          for (let j = 0; j < residues.length; j++) {

            if (residues[j].residue_number >= segRes[pointer - 1] && residues[j].residue_number <= segRes[pointer]) {
              pdbResPos.push('-');
              pdbResNum.push('-');
              pdbAutRes.push('-');
            }

            if (flag && (residues[j].residue_number === segRes[pointer]) ){

              flag = false;
              // pdbResPos.push(j);

              pdbResNum.push(residues[j].residue_number);
              pdbAutRes.push(residues[j].author_residue_number);
              for (let k = j + 1; k <= segRes[pointer + 1]; k++) {
                pdbResPos.push(k);
                if (residues[k]) {
                  pdbResNum.push(residues[k].residue_number);
                  pdbAutRes.push(residues[k].author_residue_number);
                }
              }
              pointer += 2;
            }
            flag = true;
          }
        }
      }
    }

    obj.pdb_res_pos = pdbResPos;
    obj.pdb_res_num = pdbResNum;
    obj.pdb_aut_res = pdbAutRes;
  };

}
