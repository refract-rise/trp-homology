import {RegionEntity} from './pdbEntity.interface';

export interface DataFetcher {
  uniprots: {[id: string]: UniprotInfo};
  pdbs: {[pdb: string]: PdbInfo};
}

/** Uniprots info */
export interface UniprotInfo {
  seq: string;
  pdbs: PdbsDict;
  id: string;
}

export interface PdbsDict {
  [pdb: string]: Array<string>;
}

/** Pdbs info */
export interface PdbInfo {
  uniprots: Set<string>;
  chains: {[chain: string]: ChainInfo};
}

export interface Segment {
  unp_start: number;
  unp_end: number;
  start_res_num: number;
  end_res_num: number;
}

/** Chain info */
export interface ChainInfo {
  entity_id: number;
  chain_id: string;
  struct_asym_id: string;
  regions: Array<RegionEntity>;
  segments: Array<Segment>;
  pdb_res_pos: Array<number>;
  pdb_res_num: Array<string>;
  pdb_aut_res: Array<string>;
}
