import {Component, OnInit} from '@angular/core';
import {JsonDataModel} from '../jsonData/jsonData.model';

@Component({
  selector: 'app-multifasta',
  templateUrl: './multifasta.component.html',
  styleUrls: ['./multifasta.component.css']
})

export class MultifastaComponent implements OnInit {


  multifasta;

  ngOnInit(): void {
    this.multifasta = JsonDataModel.multifasta;
    document.getElementsByClassName('multifasta')[0].innerHTML = this.multifasta;
  }
}
