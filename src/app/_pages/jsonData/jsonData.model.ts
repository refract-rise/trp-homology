import {ChainInfo, JsonData, Pdbs, Region, Uniprot} from './jsonData.interface';
import {DomSanitizer} from '@angular/platform-browser';
import {FtModel} from '../repKB-UI/featureViewer/featureViewer.model';


export class JsonDataModel {

  static dataTxt: string;
  static json: JsonData;
  static multifasta = '';

  constructor(private sanitizer: DomSanitizer) {
  }

  public getJson(data , id) {


    const uniprot: Uniprot = {id, seq: data.uniprots[id]};
    const pdbs: Pdbs = {};

    // tslint:disable-next-line:forin
    for (const pdb in data.pdbs) {
      pdbs[pdb] = {};
      // tslint:disable-next-line:forin
      for (const ch in data.pdbs[pdb].chains) {

        let chInfo: ChainInfo;
        chInfo = {
        unp_start: data.pdbs[pdb].chains[ch].unp_start,
        unp_end: data.pdbs[pdb].chains[ch].unp_end,
        pdb_indexes: data.pdbs[pdb].chains[ch].pdb_aut_res
       };

        if (data.pdbs[pdb].chains[ch].regions) {

          const arrReg = [];

          for (const region of data.pdbs[pdb].chains[ch].regions) {
            const reg: Region = {};
            reg.classification = region.classification;
            const units = FtModel.convertEntities(
              region.units, data.pdbs[pdb].chains[ch]
            ).convertedEntities;
            if (units.length > 0) {
              reg.units = units;
              reg.start = reg.units[0].x;
              reg.end = reg.units[reg.units.length - 1].y;
            }
            const insertions = FtModel.convertEntities(
              region.insertions, data.pdbs[pdb].chains[ch]
            ).convertedEntities;
            if (insertions.length > 0) {
              reg.insertions = insertions;
            }
            arrReg.push(reg);
          }
          chInfo.repeatsDb_reg = arrReg;
          pdbs[pdb][ch] = chInfo;
        }
      }
    }
    JsonDataModel.json = {uniprot, pdbs};
    JsonDataModel.dataTxt =  JSON.stringify(JsonDataModel.json, null, '\t');
    const blob = new Blob([JsonDataModel.dataTxt], {type: 'application/octet-stream'});
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  public createMultifasta(ft, multicustom, seq) {
    let multifastaStr = '';
    JsonDataModel.multifasta = '';
    if (ft === undefined) {
      return;
    }
    let mf;
    let sequence;
    for (const e of multicustom) {
      let sq = '';
      for (let i = e.x - 1; i <= e.y - 1; i++) {
        sq += seq[i];
      }
      switch (e.feature) {
        case FtModel.type.userUni:{
          ft = 'unit';
          break;
        }
        case FtModel.type.userIns:{
          ft = 'insertion';
          break;
        }
      }

      sequence = this.insertSpace(sq, 90);
      JsonDataModel.multifasta += '> ' + ft.toUpperCase() + ' ' + e.x + '-' + e.y  + sequence + '<br>';
      multifastaStr += '> ' + ft.toUpperCase() + ' ' + e.x + '-' + e.y  + sequence + '\n';
    }
    const blob = new Blob([JsonDataModel.multifasta], {type: 'application/octet-stream'});
    const blobFile = new Blob([multifastaStr], {type: 'application/octet-stream'});
    mf = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    const mff = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blobFile));

    return [mf, mff];
  }

  public insertSpace(str, n) {
    let newStr = '';
    for (let i = 0; i < str.length; i++) {

      if (i % n === 0) {

        newStr += '\n';
      }
      newStr += str[i];
    }
    return newStr;
  }

}
