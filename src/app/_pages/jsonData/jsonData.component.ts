import {Component, OnInit} from '@angular/core';
import {JsonDataModel} from './jsonData.model';

@Component({
  selector: 'app-download',
  templateUrl: './jsonData.component.html',
  styleUrls: ['./jsonData.component.css']
})

export class JsonDataComponent implements OnInit {

  json;


  ngOnInit(): void {
    this.json = JsonDataModel.dataTxt;
    this.json= this.syntaxHighlight(this.json);
    document.getElementsByClassName('json')[0].innerHTML = this.json;
  }


  syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
       (match) => {
      let cls = 'number';
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = 'key';
        } else {
          cls = 'string';
        }
      } else if (/true|false/.test(match)) {
        cls = 'boolean';
      } else if (/null/.test(match)) {
        cls = 'null';
      }
      return '<span class="' + cls + '">' + match + '</span>';
    });
  }
}
