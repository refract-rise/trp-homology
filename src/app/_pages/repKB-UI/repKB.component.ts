import {AfterViewChecked, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FeatureViewer} from 'feature-viewer-typescript/lib';
import {DomSanitizer} from '@angular/platform-browser';
import {DataFetcher, UniprotInfo} from '../../dataFetcher/interfaces/dataFetcher.interface';
import {DataFetcherModel} from '../../dataFetcher/models/dataFetcher.model';
import {JsonDataModel} from '../jsonData/jsonData.model';
import {StrucViewComponent} from './structureViewer/struc-view.component';
import {FtModel} from './featureViewer/featureViewer.model';
import {Log} from '../../_helpers/log/log.model';
import {pluck, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {Clicked, Values} from './repKB.interface';
import {RepKbClModel} from './repKB.model';

@Component({
  selector: 'app-reupro',
  templateUrl: './repKB.component.html',
  styles: ['@import "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css";'],
  styleUrls: ['./repKB.component.css']
})

export class RepKBComponent implements OnInit, AfterViewChecked {
  constructor(private route: ActivatedRoute,
              private san: DomSanitizer,
              private cg: ChangeDetectorRef,
              private stvComp: StrucViewComponent) {
    const destroyed = new Subject<any>();
    this.route.params
      .pipe(takeUntil(destroyed), pluck('id'))
      .subscribe(id => this.unpId = id);

    this.dataFetcher = new DataFetcherModel();
    this.jsonData = new JsonDataModel(san);

  }

  @Input() event: Event;

  unpId: string;
  currUnp: UniprotInfo;
  featureList: Array<any>;
  dataFetcher: DataFetcherModel;
  jsonData: JsonDataModel;
  ftv: FeatureViewer;
  _pdb: Values;
  _unp: Values;
  stv: Clicked;
  sqv: Clicked;
  data;
  pdb;
  chain;
  lastClicked;
  multicustom = [];
  lastSelectCus;
  arrEntryStv ;
  arrEntrySqv;
  actualPdb;
  unitsCus = [];
  insCus = [];
  feature;

  // variables strictly bounded with the html template
  dt;
  input;
  modules: any[];
  colorations: any = null;
  features: any[];
  ft: any = null;
  startUsrPdb;
  endUsrPdb;
  startUsrUnp;
  endUsrUnp;
  alert;
  error;
  removeOpt;


  static setUserInput(display){
    // no user input found
    if (!display) {
      display =  '-';
    }
    return display;
  }

  ngOnInit(): void {
    this.featureList = [];
    this.loadModules();
    this.updateView();
  }

  /**  Form fields: real time user values conversion */
  ngAfterViewChecked(): void {

    // when a pdb is clicked...
    if (this.pdb !== undefined && this.data.pdbs[this.pdb] !== undefined) {
      const conv = this.data.pdbs[this.pdb].chains[this.chain].pdb_aut_res;
      // lock pdb field when inserting unp and viceversa
      this._pdb.disSt = null; this._pdb.disEnd = null; this._unp.disSt = null; this._unp.disEnd = null;

      if(this.startUsrPdb || this.endUsrPdb) {
        this._unp.disSt = true;
        this._unp.disEnd = true;
      } else if (this.startUsrUnp || this.endUsrUnp) {
        this._pdb.disSt = true;
        this._pdb.disEnd = true;
      }

      // user input: PDB TO UNP
      this._unp.st = FtModel.getUnpVal(this.startUsrPdb, conv);
      this._unp.end = FtModel.getUnpVal(this.endUsrPdb, conv);
      // user input: UNP TO PDB
      this._pdb.st = FtModel.getPdbVal(this.startUsrUnp, conv);
      this._pdb.end = FtModel.getPdbVal(this.endUsrUnp, conv);

      // no user input
      this._unp.st = RepKBComponent.setUserInput(this._unp.st);
      this._unp.end = RepKBComponent.setUserInput(this._unp.end);
      this._pdb.st = RepKBComponent.setUserInput(this._pdb.st);
      this._pdb.end = RepKBComponent.setUserInput(this._pdb.end);

      this.cg.detectChanges();
    }
  }

  /** Initialize various functionalities */
  loadModules() {
    // array of colors
    this.arrEntryStv = [];
    this.sqv = { chains: [], units: [], insertions: [], userUni: [], userIns: []};
    this.stv = { chains: [], units: [], insertions: [], userUni: [], userIns: []};

    // user fields
    this._pdb = {disSt: true, disEnd: true, st: '-', end: '-'};
    this._unp = {disSt: true, disEnd: true, st: '-', end: '-'};

    // user palette
    this.modules = [];
    this.modules.push({moduleName:'Choose palette',_id:1});
    this.modules.push({moduleName:'clustal',_id:2});
    this.colorations = 1;

    // user form
    this.features = [];
    this.features.push({moduleName:'Choose feature',_id:1});
    this.features.push({moduleName:'unit',_id:2});
    this.features.push({moduleName:'insertion',_id:3});
    this.ft = 1;


    // data to download or display
    this.dt = { json: undefined, multifasta: undefined, multifastaStr: undefined };

    // errors
    this.alert = { start: 'Click on a pdb to start', checkInput: 'Invalid action. Please check the input values.',
                   selectFeature: 'Invalid action. Please select a feature.', shortFeature: 'Feature is too short to be showed.',
                   noFv:'nothing to draw on.', invalidReg: 'Start or end region are not valid',
                   notVisible: 'this feature is not fully visible on the selected pdb structure'};
    this.error = this.alert.start;

    // remove options
    this.removeOpt = { selected: 'selected', last: 'last'};
  }

  /** after retrieving data, load the elements of the page */
  public updateView() {

      this.dataFetcher.getData(this.unpId).then((data: DataFetcher) => {
      if (!data) {
        return;
      }
      this.data = data;
      this.currUnp = data.uniprots[this.unpId];

      // new Feature Viewer
      document.getElementById('fv').innerHTML = '';
      delete this.ftv;
      this.ftv = new FeatureViewer(this.currUnp.seq, '#fv', FtModel.fvOptions);
      this.featureList = FtModel.createFeatures(data, this.currUnp);
      this.ftv.addFeatures(this.featureList);
      this.ftv.onRegionSelected(r => this.updateTools(r));
      this.ftv.onButtonSelected(r => this.paint(r));

      // remove loader spin
      document.getElementsByClassName('loader')[0].className = '';
      document.getElementsByClassName('loaderMsg')[0].innerHTML = '';

      // update Sequence Viewer
      this.input = { rows: { 1: {data: this.currUnp.seq} }, colors: {}, parameters: { chunkSize: '5', log: 'none'} };

      // create json data to jsonData
      this.dt.json = this.jsonData.getJson(data, this.unpId);
    });
  }

  insertCustom (idElem, idCus) {

    idElem += 1;
    switch (idCus) {
      case FtModel.type.userUni : {
        this.unitsCus.push({x:+this._unp.st, y:+this._unp.end, id: idElem});
        break;
      }
      case FtModel.type.userIns : {
        this.insCus.push({x:+this._unp.st, y:+this._unp.end, id: idElem});
        break;
      }
    }

    let ft;
    ft = FtModel.buildCus(this._unp.st, this._unp.end, this.actualPdb, idCus, idElem);

    if (ft) {
      let  flag = false;

      for (const e of this.featureList) {
        if (e.id[0] === idCus) {
          flag = true;
          e.data.push(ft.data[0]);
        }
      }
      // if custom unit was never added to the ftv before..
      if (!flag) {
        this.featureList.unshift(ft)
      }

      this.multicustom.push({id: ft.data[0].label, pdb: this.actualPdb,
        x: +this._unp.st, y: +this._unp.end, color: FtModel.colorsHex.customUni, feature: this.feature});
    }
    this.ftv.emptyFeatures();
    this.ftv.addFeatures(this.featureList);
    if (ft && ft.data.length > 0) {
      this.ftv.highlightRegion({x: ft.data[ft.data.length - 1].x,
        y: ft.data[ft.data.length - 1].y}, ft.id);
    }
    return idElem;
  }

  /** check errors on user input */
  errCusAdd() {
    this.error = '';
    if (this.featureList.length <= 0) {
      Log.w(1, this.alert.noFv);
      return true;
    }

    if (this.feature === undefined) {
      this.error = this.alert.selectFeature;
      return true;
    }
    if (this._pdb.disSt === null && (this._unp.st === '-' || this._unp.end === '-')) {
      this.error = this.alert.checkInput;
      return true;
    } else if (this._unp.disSt === null && (this.startUsrUnp === undefined || this.endUsrUnp === undefined)) {
      this.error = this.alert.checkInput;
      return true;
    }

    if (this._pdb.disSt) {
      this._unp.st = this.startUsrUnp;
      this._unp.end = this.endUsrUnp;
    }

    if (this._unp.st === '-' || this._unp.end === '-' || +this._unp.st > +this._unp.end) {
      this.error = this.alert.checkInput;
      return true;
    } else if (+this._unp.st === +this._unp.end) {
      this.error = this.alert.shortFeature;
      return true;
    }

    if (isNaN(+this._unp.st)|| isNaN(+this._unp.end)) {
      this.error = this.alert.invalidReg;
      return true;
    } else {
      if(+this._unp.st <= 0 || +this._unp.end <= 0) {
        this.error = this.alert.invalidReg;
        return true;
      }
    }

    if (+this._unp.st >= this.currUnp.seq.length || +this._unp.end >= this.currUnp.seq.length) {
      this.error = this.alert.notVisible;
      return true;
    }
  }

  /** button add ---> add a new custom feature to FV */
  addCustom() {

    const error = this.errCusAdd();
    if (error) {return;}


    switch(this.feature) {
      case FtModel.type.userUni: {
        FtModel.idCustomUnit = this.insertCustom(FtModel.idCustomUnit, FtModel.type.userUni);
        RepKbClModel.hideBrush(this._unp.st, this._unp.end, this.unitsCus, FtModel.paint.unitCus);
        break;
      }
      case FtModel.type.userIns: {
        FtModel.idCustomIns = this.insertCustom(FtModel.idCustomIns, FtModel.type.userIns);
        RepKbClModel.hideBrush(this._unp.st, this._unp.end, this.insCus, FtModel.paint.insCus);
        break;
      }
    }
    RepKbClModel.oneElemHideBrush(this.unitsCus, this.insCus, RepKbClModel.visibility);
    // first multifasta is for the view on new page, second is for the download file
    [this.dt.multifasta, this.dt.multifastaStr] = this.jsonData.createMultifasta(this.feature, this.multicustom, this.currUnp.seq);
  }

  /** button remove ---> remove a custom feature from FV */
  removeCustom(type: string) {

    if (this.featureList.length <= 0) {
      Log.w(1, this.alert.noFv);
      return;
    }

    if (type === this.removeOpt.selected) {
      for (let i = 0; i < this.multicustom.length; i++) {
        if (this.multicustom[i].id === this.lastSelectCus.id) {
          this.multicustom.splice(i, 1);
        }
      }
      switch (this.lastSelectCus.feature[0]) {
        case FtModel.type.userUni:{
          this.removeElement(this.lastSelectCus, this.lastSelectCus.x, this.lastSelectCus.y,
                             FtModel.type.userUni, FtModel.paint.unitCus, this.unitsCus);
          break;
        }
        case FtModel.type.userIns:{
          this.removeElement(this.lastSelectCus, this.lastSelectCus.x, this.lastSelectCus.y,
                             FtModel.type.userIns, FtModel.paint.insCus, this.insCus);
          break;
        }
      }
    } else if (type === this.removeOpt.last) {
      const last = this.multicustom.pop();
      if(!last) {
        return;
      }
      switch (last.feature) {
        case FtModel.type.userUni:{
          this.removeElement(last, last.x, last.y,
                             FtModel.type.userUni, FtModel.paint.unitCus, this.unitsCus);
          break;
        }
        case FtModel.type.userIns:{
          this.removeElement(last, last.x, last.y,
                             FtModel.type.userIns, FtModel.paint.insCus, this.insCus);
          break;
        }
      }

    } else {
      this.multicustom = [];
      this.unitsCus = [];
      this.insCus = [];
      this.stv.userUni = [];
      this.stv.userIns = [];
      this.stvComp.deleteColor(this.arrEntryStv, this.stv);
      this.sqv.userIns = [];
      this.sqv.userUni = [];
      this.input = RepKbClModel.updateInput(this.arrEntrySqv, this.sqv, this.currUnp);
      for (const ft of this.featureList) {
        if (ft.id[0] === FtModel.type.userUni || ft.id[0] === FtModel.type.userIns) {
          this.featureList.splice(+ft, 1)
        }
      }
      this.ftv.emptyFeatures();
      this.ftv.addFeatures(this.featureList);
    }
    RepKbClModel.oneElemHideBrush(this.unitsCus, this.insCus, RepKbClModel.visibility);
    this.jsonData.createMultifasta(this.feature, this.multicustom, this.currUnp.seq);
  }

  removeSelected(element, check, arrCus) {

    let flag = false;
    for (const ft of this.featureList) {
      if (ft.id[0] === check) {
        flag = true;
        for (let i = 0; i < ft.data.length; i++) {
          if (ft.data[i].label === element.id) {
            ft.data.splice(i, 1);
            // TODO if data length == 0 remove ft
            i--;
            break;
          }
        }
      }
    }

    for (const key in this.stv) {
      if (key === RepKbClModel.keys.userUni || key === RepKbClModel.keys.userIns) {
        for (let i = 0; i < this.stv[key].length; i++) {
          if (this.stv.userUni[i].start_residue_number === element.x
            &&  this.stv.userUni[i].end_residue_number === element.y) {
            this.stv.userUni.splice(i, 1);
            i--;
          }
        }
      }
    }

    for (const key in this.sqv) {
      if (key === RepKbClModel.keys.userUni || key === RepKbClModel.keys.userIns) {
        for (let i = 0; i < this.sqv[key].length; i++) {
          const reg = element.x + '-' + element.y;
          if (this.sqv.userUni[i].reg === reg) {
            this.sqv.userUni.splice(i, 1);
            i--;
          }
        }
      }
    }

    for (let i = 0; i < arrCus.length; i++) {
      if (arrCus[i].id === element.id) {
        arrCus.splice(i, 1);
        i--;
      }
    }
    this.stvComp.deleteColor(this.arrEntryStv, this.stv);
    this.input = RepKbClModel.updateInput(this.arrEntrySqv, this.sqv, this.currUnp);
  }

  removeElement(last, st, end, id, pt, el){
    this.removeSelected(last, id, el);
    this.ftv.emptyFeatures();
    this.ftv.addFeatures(this.featureList);
    this.ftv.resetHighlight();
    RepKbClModel.hideBrush(st, end, el, pt);
    this.stvComp.deleteColor(this.arrEntryStv, this.stv);
    this.input = RepKbClModel.updateInput(this.arrEntrySqv, this.sqv, this.currUnp);
  }

  private paint(event) {

    const ch = this.lastClicked[this.lastClicked.length - 1];
    const pdb = this.lastClicked.slice(0, -2);
    this.stv.userUni = [];
    this.stv.userIns = [];
    this.sqv.userIns = [];
    this.sqv.userUni = [];
    this.eraseAll();

    switch (event.detail.tooltip) {
      case FtModel.paint.unitCus: {
        for (const ft of this.featureList) {

          if (ft.id[0] === FtModel.type.userUni) {
            this.updateEntity(ft.data, pdb, ch, FtModel.type.userUni);
          }
        }
        break;
      }
      case FtModel.paint.insCus: {
        for (const ft of this.featureList) {
          if (ft.id[0] === FtModel.type.userIns) {
            this.updateEntity(ft.data, pdb, ch, FtModel.type.userIns);
          }
        }
        break;
      }
      case FtModel.paint.unit: {
        const xy = JSON.parse(event.detail.dataxy);
        this.updateEntity(xy, pdb, ch, FtModel.type.units);
        break;
      }
      case FtModel.paint.ins: {
        const xy = JSON.parse(event.detail.dataxy);
        this.updateEntity(xy, pdb, ch, FtModel.type.insertions);
        break;
      }
    }
  }

  updateEntity(xy, pdb, ch, idt){
    for (const entity of xy) {
      const cl = RepKbClModel.hexToRgb(entity.color);
      this.updateSqv(entity.x, entity.y, idt, entity.color);
      this.updateStv(entity.x, entity.y, pdb, ch, idt, {r: cl.r, g: cl.g, b: cl.b}, xy.length);
    }
  }

  updateTools(r) {

    this.error = '';
    // preprocess input
    const x = r.detail.selectedRegion.x;
    const y = r.detail.selectedRegion.y;

    let pdb;
    let ch;
    let identity;
    let rgb;

    const clickedColorHex = r.detail.selectedRegion.color;
    const clickedColorRgb = RepKbClModel.hexToRgb(clickedColorHex);

    let label = r.detail.label;
    const xy = -1;
    // if custom label, take name of last clicked pdb
    if (r.detail.id[0] === FtModel.type.userUni || r.detail.id[0] === FtModel.type.userIns) {
      this.error = '';
      if (clickedColorHex === FtModel.colorsHex.transp) {
        return;
      }


      this.lastSelectCus = {id: r.detail.selectedRegion.label, pdb: r.detail.label, x, y, feature: r.detail.id};
      label = r.detail.id[0] + '-' + this.lastClicked;

    }

    identity = FtModel.type.chains;

    switch (label[0]) {
      case FtModel.type.units: {
        label = label.substring(2);
        identity = FtModel.type.units;
        break;
      }
      case FtModel.type.insertions: {
        label = label.substring(2);
        identity = FtModel.type.insertions;
        break;
      }
      case FtModel.type.userUni: {
        label = label.substring(2);
        identity = FtModel.type.userUni;
        break;
      }
      case FtModel.type.userIns: {
        label = label.substring(2);
        identity = FtModel.type.userIns;
        break;
      }
    }
    this.lastClicked = label;

    rgb = {r: clickedColorRgb.r, g: clickedColorRgb.g, b: clickedColorRgb.b};
    [pdb, ch] = label.split('-');
    this.pdb = pdb;
    this.chain = ch;
    const clickedPdb = pdb + '-' + ch;
    if (this.actualPdb !== undefined ) {
      if (this.actualPdb !== clickedPdb){
        [this.arrEntryStv, this.arrEntrySqv, this.stv, this.sqv] =
          RepKbClModel.emptyArr(this.arrEntryStv, this.arrEntrySqv, this.stv, this.sqv);
      }
    }

    this.updateStv(x, y, pdb, ch, identity, rgb, xy);
    this.updateSqv(x, y, identity, clickedColorHex);
    this.actualPdb = clickedPdb;
  }

  updateStv(st, end, pdb, ch, identity, rgb, xy) {
    const chains = this.data.pdbs[pdb].chains;
    const startClicked = st;
    const endClicked = end;

    if (identity === FtModel.type.userUni || identity === FtModel.type.userIns) {
      // coloring all chains of the clicked pdb
      for (const chain in this.data.pdbs[pdb].chains) {

        // coloring structure viewer
        if (st !== undefined && end !== undefined) {

          // Litemol wants res_num, even though it shows aut_res

          st = this.data.pdbs[pdb].chains[chain].pdb_res_num[startClicked - 1];
          end = this.data.pdbs[pdb].chains[chain].pdb_res_num[endClicked - 1];

          const stvInfo = RepKbClModel.createStvInfo(rgb, chains, this.data.pdbs[pdb].chains[chain].chain_id, st, end);
          this.arrEntryStv = this.stvComp.updateView(xy, this.arrEntryStv, this.stv,
            pdb.toLowerCase(),
            this.data.pdbs[pdb].chains[chain].chain_id,
            identity, // region or units/insertions
            stvInfo
          );
        }
      }
    } else {
      // coloring structure viewer - convert from uniprot values to pdb res_num
      st = this.data.pdbs[pdb].chains[ch].pdb_res_num[startClicked - 1];
      end = this.data.pdbs[pdb].chains[ch].pdb_res_num[endClicked - 1];

      // TODO correcting ebi wrong data for uniprot Q13835
      //  better would be to check data during retrival and try to correct them there
      if (pdb === '1xm9' && identity === 'c') {
        end = 457;
      }

      const stvInfo = RepKbClModel.createStvInfo(rgb, chains, ch, st, end);
      this.arrEntryStv = this.stvComp.updateView(xy, this.arrEntryStv, this.stv,
        pdb.toLowerCase(),
        ch,
        identity, // region or units/insertions
        stvInfo);
    }
  }

  updateSqv(st, end, identity, cl) {
    const reg = st + '-' + end;
    const obj = {reg, cl};
    RepKbClModel.insElem(identity, this.arrEntrySqv, obj, this.sqv, 'sqv');
    this.input = RepKbClModel.updateInput(this.arrEntrySqv, this.sqv, this.currUnp);

  }

  selectedFeature (event: any) {
    if (event === '2') {
      this.feature = FtModel.type.userUni;
    } else {
      this.feature = FtModel.type.userIns;
    }
  }

  selectPalette(event: any) {
    if (event === '2') {

      this.input = {
        rows: {
          1: {data: this.currUnp.seq}
        },
        colors: {
          '@amino@': [
            {row: '1', color: '@adjacent', target: 'background'}
          ]
        },
        parameters: { chunkSize: '5', log: 'debug'}
      };
    }
  }

  restore() {
    this.input = RepKbClModel.updateInput(this.arrEntrySqv, RepKbClModel.lastColorsSqv, this.currUnp);
    this.colorations = 1;
  }

  eraseAll() {
    [this.arrEntryStv, this.arrEntrySqv, this.stv, this.sqv] =
      RepKbClModel.emptyArr(this.arrEntryStv, this.arrEntrySqv, this.stv, this.sqv);
    this.stvComp.deleteColor(this.arrEntryStv, this.stv);
    this.input = RepKbClModel.updateInput(this.arrEntrySqv, this.sqv, this.currUnp);
  }
}
