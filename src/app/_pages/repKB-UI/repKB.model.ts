import {Stv} from './repKB.interface';
import {FtModel} from './featureViewer/featureViewer.model';
import {Visibility} from './repKB.interface';

export class RepKbClModel {

  static visibility:Visibility = {visible: 'visible', hidden: 'hidden'};
  static lastPaintUnit = 'visible';
  static lastPaintIns = 'visible';
  static lastColorsSqv;
  static tool = { sqv: 'sqv', stv: 'stv'};
  static keys = { userUni: 'userUni', userIns: 'userIns'};


  static hideBrush(st, end, arrCus, idPaint){

    // checking superposed features
    // if present, remove paint brush
    let insert = true;
    for (const j of arrCus) {
      let c = 0;
      for (const i of arrCus) {
        if (i.x >= j.x && i.x <= j.y || i.y >= j.x && i.y <= j.y) {
          c += 1;
          if (c > 1) {
            insert = false;
            break;
          }
        }
      }
    }

    if(insert){
      if (document.getElementById(idPaint)) {
        document.getElementById(idPaint).style.visibility = RepKbClModel.visibility.visible;

      }
    } else {
      document.getElementById(idPaint).style.visibility = RepKbClModel.visibility.hidden;
    }

    switch (idPaint) {
      case FtModel.paint.unitCus: {
        RepKbClModel.lastPaintUnit = RepKbClModel.changeVisibility(idPaint, FtModel.paint.unitCus,
          FtModel.paint.insCus, RepKbClModel.lastPaintIns);
        break;
      }
      case FtModel.paint.insCus: {
        RepKbClModel.lastPaintIns = RepKbClModel.changeVisibility(idPaint, FtModel.paint.insCus,
          FtModel.paint.unitCus, RepKbClModel.lastPaintUnit);
        break;
      }
    }
  }

  static changeVisibility(idPaint, ptFirst, ptSecond,lPtSecond) {
    let lstPtFirst;
    if (document.getElementById(idPaint)) {
      lstPtFirst = RepKbClModel.lastPaintUnit = document.getElementById(ptFirst).style.visibility;
      if (document.getElementById(ptSecond)) {
        document.getElementById(ptSecond).style.visibility = lPtSecond;
      }
    }
    return lstPtFirst;
  }

  static oneElemHideBrush(uni, ins, vis){
    if (uni.length <= 1) {
      if (document.getElementById(FtModel.paint.unitCus)) {
        document.getElementById(FtModel.paint.unitCus).style.visibility = vis.hidden;
      }
    }
    if (ins.length <= 1) {
      if (document.getElementById(FtModel.paint.insCus)) {
        document.getElementById(FtModel.paint.insCus).style.visibility = vis.hidden;
      }
    }
  }

  static updateInput(arrEntrySqv, sqv, unp) {
    arrEntrySqv = RepKbClModel.pushArr(arrEntrySqv, sqv, RepKbClModel.tool.sqv);
    const colors = {};

    for (const e of arrEntrySqv) {
      colors[e.reg] = [];
      colors[e.reg].push({row: 1, color: e.cl});
    }

    return  {
      rows: {
        1: {data: unp.seq}
      },
      colors,
      parameters: { chunkSize: '5', log: 'none'}
    };
  }

  static createStvInfo (rgb, chains, ch, st, end) {
    const stvInfo: Stv = {
      entity_id: '',
      struct_asym_id: '',
      start_residue_number: 0,
      end_residue_number: 0,
      color: rgb
    };
    stvInfo.entity_id = chains[ch].entity_id.toString();
    stvInfo.struct_asym_id = chains[ch].struct_asym_id;
    stvInfo.start_residue_number = st;
    stvInfo.end_residue_number = end;
    return stvInfo;
  }

  static insElem(id, arr, obj, ck, tool) {

    for( const type of Object.keys(FtModel.type)) {

      if (FtModel.type[type] === id) {
        RepKbClModel.insertClickElem(obj, ck[type], tool);
      }
    }
  }

  static insertClickElem(obj, arr, condition) {

    let elem;
    let flag = true;
    for (let i = 0; i < arr.length; i++) {
      elem = arr[i];
    switch(condition){
      case RepKbClModel.tool.stv: {
        if ((obj.struct_asym_id !==  elem.struct_asym_id)
          || (obj.start_residue_number !==  elem.start_residue_number)
          || (obj.end_residue_number !==  elem.end_residue_number)
          || ((JSON.stringify(obj.color) !== JSON.stringify(elem.color)))) {
          continue;
        }
        break;
      }
      case RepKbClModel.tool.sqv: {

        if ((obj.reg !==  elem.reg) && (obj.color !==  elem.reg)) {
          continue;
        }
        break;
      }
    }
      // Delete current element and break
      arr.splice(i, 1);
      flag = false;
      break;
    }
    if (flag) {
      delete obj.pdb;
      arr.push(obj);
    }

  }

  static pushArr(arr, ck, tool) {

    arr = [];
    for( const type of Object.keys(FtModel.type)) {
      Array.prototype.push.apply(arr, ck[type]);
    }

    if (tool === RepKbClModel.tool.sqv) {
      RepKbClModel.lastColorsSqv = ck;
    }
    return arr;
  }

  static emptyArr(arrEntryStv, arrEntrySqv, stv, sqv) {
    arrEntryStv = [];
    arrEntrySqv = [];

    for( const type of Object.keys(FtModel.type)) {
      stv[type] = [];
      sqv[type] = [];
    }

    return [arrEntryStv, arrEntrySqv, stv, sqv]
  }

  static hexToRgb(hex) {

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

}
