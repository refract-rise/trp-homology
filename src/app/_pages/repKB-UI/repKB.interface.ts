export interface Clicked {
  chains: Array<any>,
  units: Array<any>,
  insertions: Array<any>,
  userUni: Array<any>,
  userIns: Array<any>
}

export interface Stv {
  entity_id: string,
  struct_asym_id: string,
  start_residue_number: number,
  end_residue_number: number,
  color: {r: number, g: number, b:number}
}

export interface Values {
  disSt: boolean,
  disEnd: boolean,
  st: string,
  end: string
}

export interface Visibility {
  visible: string,
  hidden: string
}
