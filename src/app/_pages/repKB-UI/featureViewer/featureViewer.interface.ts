export interface Type {
  chains: string,
  units: string,
  insertions: string,
  unp: string,
  userUni: string,
  userIns: string
}
