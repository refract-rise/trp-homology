import {ChainInfo, PdbInfo, Segment} from '../../../dataFetcher/interfaces/dataFetcher.interface';
import {Entity} from '../../../dataFetcher/interfaces/pdbEntity.interface';
import {Clicked} from '../repKB.interface';
import {Type} from './featureViewer.interface';
import random = LiteMol.Visualization.Color.random;

export class FtModel {

  static unpUrl = 'https://www.uniprot.org/uniprot/';
  static chaUrl = 'http://www.rcsb.org/structure/';
  static pdbUrl = 'http://repeatsdb.bio.unipd.it/protein/';
  static fvOptions = {
    showAxis: true, showSequence: true, toolbar: true,
    toolbarPosition: 'left', zoomMax: 10, sideBar: 200,
    flagColor: '#DFD5F5', showSubFeatures: true, backgroundcolor: 'white',
    flagTrack: 130,
    flagTrackMobile: 110
  };

  static colorsHex = {
    uniprot: '#70B77E',
    chains: '#D62839',
    unitsLight: '#00709B',
    unitsDark: '#03256C',
    insertions: '#F2BB05',
    customUni: '#1C7C54',
    cOne: '#E36414',
    cTwo: '#8D6A9F',
    transp:  '#FFFFFFFF'
  };
  static custom = {
    idUnit: 'custom-unit',
    idIns: 'custom-insertion'
  };
  static idCustomUnit = 0;
  static idCustomIns = 0;

  static paint = {
    unit: 'paint-unit',
    unitCus: 'paint-custom-unit',
    ins: 'paint-insertion',
    insCus: 'paint-custom-insertion'
  };

  static type: Type = {
    chains: 'c',
    units: 'u',
    insertions: 'i',
    unp: 'p',
    userUni: 'z',
    userIns: 'w'
  };

  /** fill Feature Viewer */
  static createFeatures(data, currUnp) {
    const featureList = [];
    featureList.push(FtModel.buildUnpFt(currUnp.id, currUnp.seq.length));

    let chFeature;
    let pdbInfo: PdbInfo;
    let chainInfo: ChainInfo;

    for (const pdb of Object.keys(currUnp.pdbs)) {
      pdbInfo = data.pdbs[pdb];
      for (const chain of currUnp.pdbs[pdb].sort()) {
        chainInfo = pdbInfo.chains[chain];

        // chains
        chFeature = FtModel.buildChFt(pdb, chainInfo);
        let subFt;
        // regions
        subFt = FtModel.buildRegFt(pdb, chainInfo, chFeature);
        if (subFt.length > 0) {
          chFeature.subfeatures = FtModel.buildRegFt(pdb, chainInfo, chFeature);
        }
        featureList.push(chFeature);
      }
    }
    return featureList;
  }

  /** Custom entities */
  static buildCus(start: string, end: string, actualPdb: string, feature: string, dtLabel: number) {
    const x = +start;
    const y = +end;

    let id;
    let label;
    let pt;

    if (feature === this.type.userUni) {
      id = this.type.userUni + '-' + actualPdb;
      label = 'units';
      pt = this.custom.idUnit;
    } else {
      id = this.type.userIns + '-' + actualPdb;
      pt = this.custom.idIns;
      label = 'insertions';
    }
    return {
      type: 'rect',
      label,
      id,
      data: [{x, y, color: this.colorsHex.customUni, label: dtLabel}],
      isOpen: true,
      sidebar: [
        {
          id: 'paint-' + pt,
          tooltip: 'paint-' + pt,
          content: `<i data-id='usr' class='fa fa-paint-brush'></i>`
        }
      ]
    };
  }

  /** Uniprot entity */
  static buildUnpFt(uniprotId: string, sequenceLength: number) {
    return {
      type: 'rect', color: this.colorsHex.uniprot,
      label: uniprotId,
      id: `${this.type.unp}-${uniprotId}`,
      data: [
        { x: 1, y: sequenceLength }
      ],
      sidebar: [
        {
          id: 'unpLink',
          content: `<a target="_blank" href="${FtModel.unpUrl}${uniprotId}"><i class="fa fa-link"></i></a>`,
        }
      ]
    };
  }

  /** Pdb chain entities */
  static buildChFt(pdb: string, chainInfo: ChainInfo) {

    const res =  {
      type: 'rect',
      label: `${pdb}-${chainInfo.chain_id}`,
      id: `${this.type.chains}-${pdb}-${chainInfo.chain_id}`,
      data: [],
      isOpen: true,
      sidebar: [
        {
          id: `pdbLink-${pdb}-${chainInfo.chain_id}`,
          tooltip: `PDB ${pdb}-${chainInfo.chain_id}`,
          content: ''
        }
      ]
    };
    for (const segment of chainInfo.segments) {
      res.data.push({x: segment.unp_start, y: segment.unp_end, color: this.colorsHex.chains});
    }
    res.sidebar[0].content =  `<a target="_blank" href="${FtModel.chaUrl}${pdb}">
                                    <i style="margin-top:5px;" class="fa fa-external-link-square" ></i></a>`;
    return res;
  }

  /** Units, insertions entities */
  static buildRegFt(pdb: string, chainInfo: ChainInfo, chFeature) {

    const result = [];
    const regions = chainInfo.regions;
    let flagAdditional = false;
    let obj;

    const convUnits = [];
    const convIns = [];
    for (const region of regions) {

      obj = FtModel.convertEntities(region.units, chainInfo);

      Array.prototype.push.apply(convUnits, obj.convertedEntities);

      if (obj.flagAdditional === false) {
        obj = FtModel.convertEntities(region.insertions, chainInfo);
        Array.prototype.push.apply(convIns, obj.convertedEntities);
      } else {
        obj = FtModel.convertEntities(region.insertions, chainInfo);
        Array.prototype.push.apply(convIns, obj.convertedEntities);
        flagAdditional = true;
      }
    }

    if (convUnits.length > 0) {
      result.push(FtModel.buildEntityFt(FtModel.custom.idUnit, pdb, chainInfo.chain_id, convUnits));
    }
    if (convIns.length > 0) {
      result.push(FtModel.buildEntityFt(FtModel.custom.idIns, pdb, chainInfo.chain_id, convIns));
    }
    // tooltip to advertise that there are additional info on repeatsdb
    // but I don't display them because they don't map on the uniprot
    if (flagAdditional) {

      chFeature.sidebar[0].content = `<a target="_blank" href="${FtModel.chaUrl}${pdb}">
                                          <i style="margin-top:5px;" class="fa fa-external-link-square" ></i></a>
                                          `;
      chFeature.sidebar.push({
        id: `rpLink ${pdb}-${chainInfo.chain_id}`,
        tooltip: `RpsDb ${pdb}-${chainInfo.chain_id}`,
        content: `<a target="_blank" href="${FtModel.pdbUrl}${pdb}${chainInfo.chain_id}">
                    <i style="margin-top: 6px" class="fa fa-external-link"></i></a>`
      });
      chFeature.sidebar[1].tooltip = pdb + chainInfo.chain_id + ' | RpsDb additional info';
    }
    return result;

  }

  private static buildEntityFt(
    feature: string, pdb: string, chain: string, data: Array<{x: number, y: number, color: string}>) {

    let label;
    let tooltip;
    switch (feature) {
      case this.custom.idUnit: {
        label = `${this.type.units}-${pdb}-${chain}`;
        tooltip = this.paint.unit;
        let flag = true;
        if (data.length > 1) {

          for (const elem of data) {
            if (flag) {
              elem.color = this.colorsHex.unitsDark;
              flag = !flag;
              continue;
            }
            elem.color = this.colorsHex.unitsLight;
            flag = !flag;
          }
        }
        break;
      }
      case this.custom.idIns: {
        label = `${this.type.insertions}-${pdb}-${chain}`;
        tooltip = this.paint.ins;
        for (const elem of data) {
          elem.color = this.colorsHex.insertions;
        }
        break;
      }
    }
    const dt = JSON.stringify(data);
    let paint = { id: `none`,
      tooltip: ``,
      dataxy: ``,
      content: `<a></a>`};

    if(data.length > 1){
      paint = {
        id: `${label}`,
        tooltip,
        dataxy: `${dt}`,
        content: `<a><i class="fa fa-paint-brush"></i></a>`
      }
    }
    return {
      type: 'rect',
      id: label,
      data,
      isOpen: true,
      sidebar: [
        {
          id: `rpLink-${pdb}-${chain}`,
          tooltip: `RpsDb ${pdb}-${chain}`,
          content: `<a target="_blank" href="${FtModel.pdbUrl}${pdb}${chain}">
                    <i class="fa fa-external-link"></i></a>`
        },
        paint
      ]
    };

  }

  public static convertEntities(entities: Array<Entity>, convObj) {
    let flagAdditional = false;
    const convertedEntities = [];
    let start;
    let end;

    for (const entity of entities) {

      start = FtModel.getUnpVal(entity.start, convObj.pdb_aut_res);
      end = FtModel.getUnpVal(entity.end, convObj.pdb_aut_res);

      if (start && end === 0|| start === 0 && end) {
        // Log.w(1, 'no conversion value found.');
        flagAdditional = true;
      }

      // if the entities are outside pdb, even for a little residues, I don't draw them
      // but I place the additional info tooltip
      for (const seg of convObj.segments) {

        if (start >= seg.unp_start && end <= seg.unp_end) {
          convertedEntities.push({x: start, y: end});
        } else {
          flagAdditional = true;
        }
      }

    }
    const obj = {
      convertedEntities,
      flagAdditional
    };
    return obj;
  }

  public static getPdbVal(unp, arr) {

    let pdb;
    if (unp in arr) {
      // shift bc array starts from 0...
      pdb = arr[unp - 1];
    }

    if (pdb === -1) {
      pdb = '-';
    }
    return pdb;
  }

  public static getUnpVal(pdb, arr) {
    let unp;
    if (pdb){
      // shift bc array starts from 0...
      unp = arr.indexOf(+pdb) + 1;
    }
    if (unp === -1) {
      unp = '-';
    }
    return unp;
  }

}
