import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AlignmentComponent} from '../_pages/alignment/alignment.component';
import {RepKBComponent} from '../_pages/repKB-UI/repKB.component';
import {JsonDataComponent} from '../_pages/jsonData/jsonData.component';
import {MultifastaComponent} from '../_pages/multifasta/multifasta.component';

const routes: Routes = [

  { path: 'repKB-UI/:id', component: RepKBComponent },
  { path: 'json', component: JsonDataComponent},
  { path: 'multifasta', component: MultifastaComponent},
  { path: '', component: AlignmentComponent }
];

@NgModule({
  imports: [RouterModule,   RouterModule.forRoot(
    routes
  )],
  exports: [RouterModule]
})

export class AppRoutingModule {}
export const routingComponents = [RepKBComponent];
