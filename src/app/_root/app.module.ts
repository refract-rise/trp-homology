import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent} from './app.component';
import { RepKBComponent } from '../_pages/repKB-UI/repKB.component';
import { AlignmentComponent } from '../_pages/alignment/alignment.component';
import {StrucViewComponent} from '../_pages/repKB-UI/structureViewer/struc-view.component';
import {SqvLibModule} from 'sqv-lib';
import {AppRoutingModule, routingComponents} from './app-routing.module';
import {NavComponent} from '../_helpers/nav/nav.component';
import {UnpFilterPipe} from '../_helpers/pipe/unp-filter.pipe';
import {JsonDataComponent} from '../_pages/jsonData/jsonData.component';
import {MultifastaComponent} from '../_pages/multifasta/multifasta.component';



@NgModule({
  declarations: [
    AlignmentComponent,
    RepKBComponent,
    StrucViewComponent,
    AppComponent,
    routingComponents,
    NavComponent,
    UnpFilterPipe,
    JsonDataComponent,
    MultifastaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SqvLibModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
